"use strict";
var request = require('supertest'),
	express = require('express');

var app = require('../../app').getConfiguredApp();

// This array contains all the get URLs. The test is the same
// for all of them so let's just loop through them

var getRoutes = ["/", "/spark", "/you", "/end-extreme-poverty", "/change-maker",
"/change-maker/country/australia", "/change-maker/category/education",
"/change-maker/bobs-pancake-truck", "/supporter/sugendran"];
getRoutes.forEach(function(url) {
	exports[url] = function(test) {
		request(app)
			.get(url)
			.expect("Content-Type", "text/html")
			.expect(200)
			.end(function(err, response) {
				test.equal(err, null);
				test.done();
			});
	};
});

var redirectedGetRoutes = ["/change-maker/country", "/change-maker/category"];
redirectedGetRoutes.forEach(function(url) {
	exports[url] = function(test) {
		request(app)
			.get(url)
			.expect("Content-Type", "text/html")
			.expect(302)
			.end(function(err, response) {
				test.equal(err, null);
				test.done();
			});
	};
});
