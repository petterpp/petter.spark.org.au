module.exports = function(sparkdb) {
    "use strict";

    function searchChangemakers(filters, callback) {
        var params = { include_docs : true };
        if (filters && filters.accelerator_id) {
            params[key] = filters && filters.accelerator_id;
        }
        sparkdb.view('changemaker', 'byAccelerator', params,
            function(err, doc) {
                if (err) {
                    return callback(err);
                }
                callback(null, doc.rows && doc.rows.map(function(row) { return row.doc; }));
            });
    }

    function getChangemaker(id, callback) {
        sparkdb.get(id, callback);
    }

    function updateOrInsertChangeMaker(changemaker, callback) {
        var docid = changemaker._id;
        getChangemaker(docid, function(err, cm) {
            if(err && err.message !== 'missing') {
                return callback(err);
            }
            if(err && err.message === 'missing') {
                cm = blankChangemaker();
            }
            if(!cm) {
                cm = blankChangemaker();
            }
            var keys = Object.keys(changemaker);
            keys.forEach(function(key) {
                if(key === '_rev') {
                    return;
                }
                cm[key] = changemaker[key];
            });
            console.log("saving changemaker %j", changemaker);
            sparkdb.insert(cm, docid, function(err, body) {
                if(err) {
                    console.log("error saving changemaker: %j", err);
                    return callback(err);
                }
                callback(null, cm);
            });
        });
    }

    function getCount(callback) {
        sparkdb.view('changemaker', 'count', { group : true }, 
            function(err, doc) {
                if(err) {
                    return callback(err);
                }
                callback(null, doc.rows[0].value);
            });
    }

    function blankChangemaker() {
        return {
            "type" : "changemaker",
            "salesforce_ref" : "",
            "first_name" : "",
            "last_name" : "",
            "story": "",
            "accelerator_id" : null,
            "media" : []
        };
    }

    return {
        count : getCount,
        search : searchChangemakers,
        get : getChangemaker,
        blank : blankChangemaker,
        upsert: updateOrInsertChangeMaker
    };
};
