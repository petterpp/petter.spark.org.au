module.exports = function(sparkdb) {
    "use strict";

    function get(salesforce_ref, type, callback) {
        if (!callback) {
            callback = type;
            type = null;
        }
        var key = [ salesforce_ref ];
        if (type) {
            key.push(type);
        }
        var params = {
            key   : key,
            include_docs : true
        };
        sparkdb.view('salesforce', 'bySalesforceRefThenType', params,
            function(err, doc) {
                if (err) {
                    return callback(err);
                }
                var obj = doc.rows && doc.rows.length && doc.rows[0].doc;
                callback(null, obj || null);
            });
    }

    function lastSyncDate(callback) {
        sparkdb.get('sparklastsync', function(err, doc){
            if(err) {
                if(err.message !== 'missing') {
                    return callback(err);
                }
                sparkdb.insert({
                    lastSync: 0,
                    locked: false,
                    syncStarted: 0,
                    type: 'lastsync'
                }, 'sparklastsync', function(err) {
                    if(err) {
                        return callback(err);
                    }
                    return lastSyncDate(callback);
                });
                return;
            }
            callback(null, doc);
        });
    }

    function updateSyncInfo(doc, callback) {
        sparkdb.insert(doc, 'sparklastsync', function(err, upInfo) {
            if(err && err.message === 'conflict') {
                lastSyncDate(function(err, dbdoc) {
                    if(err) {
                        return callback(err);
                    }
                    dbdoc.lastSync = doc.lastSync;
                    dbdoc.locked = doc.locked;
                    dbdoc.syncStarted = doc.syncStarted;
                    updateSyncInfo(dbdoc, callback);
                });
            } else if(upInfo) {
                doc._rev = upInfo.rev;
                callback(null, doc);
            } else {
                callback(err || null);
            }
        });
    }

    return {
        get : get,
        lastSync: lastSyncDate,
        updateSyncInfo: updateSyncInfo
    };
};
