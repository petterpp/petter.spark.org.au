    /*jshint strict:false*/
/*globals emit,reduce,sum*/
module.exports = {
    _id: "changemaker",
    language: "javascript",
    views: {
        byAccelerator : {
            map: function(doc) {
                if(doc.type === 'changemaker') {
                    emit(doc.accelerator_id, null);
                }
            }
        },
        count : {
            map: function(doc) {
                if(doc.type === 'changemaker') {
                    emit(null, 1);
                }
            },
            reduce: function(key, values) {
                return sum(values);
            }
        }
    }
};
