/*jshint strict:false*/
/*globals emit,reduce,sum*/
module.exports = {
    _id: "project",
    language: "javascript",
    views: {
        byCategory: {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit(doc.category, doc);
                }
            }
        },
        byFiltersThenByFunding: {
            map: function(doc) {
                if (doc.type === 'project') {
                    var goal = 0;
                    var contributions = 0;
                    for (var grant in doc.funding_grants) {
                        if (!doc.funding_grants[grant].attained_date) {
                            goal = doc.funding_grants[grant].amount_usd;
                            contributions = 0;
                            for (var contribution in doc.funding_grants[grant].contributions) {
                                contributions += doc.funding_grants[grant].contributions[contribution].amount_usd;
                            }
                            break; // only allow one active goal for now
                        }
                    }
                    var left = goal - contributions;
                    emit([ 0, doc.location, 1, doc.category, 2, left / goal, doc._id, 0 ], null);
                    emit([ 0, doc.location, 2,                  left / goal, doc._id, 0 ], null);
                    emit([ 1, doc.category, 2,                  left / goal, doc._id, 0 ], null);
                    emit([ 2,                                   left / goal, doc._id, 0 ], null);

                    emit([ 0, doc.location, 1, doc.category, 2, left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 0, doc.location, 2,                  left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 1, doc.category, 2,                  left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 2,                                   left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                }
            }
        },
        byFiltersThenByCreationDate: {
            map: function(doc) {
                if (doc.type === 'project') {
                    emit([ 0, doc.location, 1, doc.category, 2, doc.created_date, doc._id, 0 ], null);
                    emit([ 0, doc.location, 2,                  doc.created_date, doc._id, 0 ], null);
                    emit([ 1, doc.category, 2,                  doc.created_date, doc._id, 0 ], null);
                    emit([ 2,                                   doc.created_date, doc._id, 0 ], null);

                    emit([ 0, doc.location, 1, doc.category, 2, doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 0, doc.location, 2,                  doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 1, doc.category, 2,                  doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 2,                                   doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                }
            }
        },
        byFiltersThenByRecentUpdate: {
            map: function(doc) {
                if (doc.type === 'project') {
                    var max_update;
                    for (var update in doc.updates) {
                        max_update = max_update > doc.updates[update].created_date ?
                            max_update :
                            doc.updates[update].created_date;
                    }
                    emit([ 0, doc.location, 1, doc.category, 2, max_update, doc._id, 0 ], null);
                    emit([ 0, doc.location, 2,                  max_update, doc._id, 0 ], null);
                    emit([ 1, doc.category, 2,                  max_update, doc._id, 0 ], null);
                    emit([ 2,                                   max_update, doc._id, 0 ], null);

                    emit([ 0, doc.location, 1, doc.category, 2, max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 0, doc.location, 2,                  max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 1, doc.category, 2,                  max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ 2,                                   max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                }
            }
        },
        bySlugWithLinkedEntities : {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit([ doc.slug, 0 ], null);

                    //Linked Entities
                    for (var grant in doc.funding_grants) {
                        for (var contribution in doc.funding_grants[grant].contributions) {
                            emit([ doc.slug, 1 ], {
                                _id : doc.funding_grants[grant].contributions[contribution].user_id
                            });
                        }
                    }
                    for (var help in doc.help_offers) {
                        emit([ doc.slug, 2 ], {
                            _id : doc.help_offers[help].user_id
                        });
                    }
                    emit([ doc.slug, 3 ], { _id : doc.changemaker_id });
                }
            }
        },
        countsByCategory: {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit(doc.category, 1);
                }
            },
            reduce: function(key, values) {
                return sum(values);
            }
        },
        countsByLocation: {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit(doc.location, 1);
                }
            },
            reduce: function(key, values) {
                return sum(values);
            }
        }
    }
};
