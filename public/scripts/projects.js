/*globals $:false, window:false, Spark:false */
(function() {
    "use strict";

    var $projectForm = $('.project-search');
    $projectForm.on('change', 'input[type="radio"], input[type="checkbox"]', function() {
        $(this).siblings('input').addBack().each(function() {
            $(this).toggleClass('checked', this.checked);
        });
    }).on('change', ':input', function() {
        var country = $projectForm.find('[name="country"]:checked').val();
        var category = $projectForm.find('[name="category"]:checked').val();
        var order = $projectForm.find('[name="order"]').val();
        window.location = Spark.searchUrl(country, category, order);
    });

    $(document)
        .on('click', '.tabs > .tab-menu > .tab', function() {
            var $this = $(this);
            $this.addClass('selected');
            $this.siblings('.tab').removeClass('selected');
            var $pane = $this.closest('.tabs')
                            .find('> .tab-content > [aria-labelledby="' + $this.attr('id') + '"]');
            $pane.attr('aria-hidden', false);
            $pane.siblings('[role="tabpanel"]').attr('aria-hidden', true);
        })
        .on('change', '#project-actions select[name="fund_amount"]', function (e) {
            $('#project-actions .fund-button > .detail').text("$" + $(this).val() / 100);
        })
        .on('click', '#project-actions .fund-button', function(e) {
            e.preventDefault();
            var $toggledParent = $(this).closest('#project-actions');
            var $toggledOther = $toggledParent.siblings('#donation-form');

            var thisAmount = $toggledParent.find('[name="fund_amount"]').val();
            $toggledOther.find('[name="fund_amount"]').val(thisAmount);

            $toggledParent.attr('aria-hidden', true);
            $toggledOther.attr('aria-hidden', false);
        })
        .on('click', '#donation-form .cancel', function(e) {
            e.preventDefault();
            var $toggledParent = $(this).closest('#donation-form');
            var $toggledOther = $toggledParent.siblings('#project-actions');

            $toggledParent.attr('aria-hidden', true);
            $toggledOther.attr('aria-hidden', false);
        })
}());
