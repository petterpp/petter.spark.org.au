var login = require("./login");
var _ = require('lodash');
var async = require('async');
var fs = require('fs');
var db = require("../lib/db");
var mapper = require("./mapper");
var oauth;
var sf;
var nforce = login.nforce;
var individualAccountId;

var queries = JSON.parse(fs.readFileSync(__dirname + "/queries.json"));

/// sync lock stuff
var lastSyncInfo;
var syncStartDate;
function getLastSyncDate(callback) {
    db.ready(function(api) {
        api.salesforce.lastSync(function(err, doc) {
            if(err) {
                return callback(err);
            }
            lastSyncInfo = doc;

            if (typeof(lastSyncInfo) === 'undefined') {
                // there was no lastSync so it can't be locked
                lastSyncInfo = {
                    locked: false,
                    syncStarted: null,
                    lastSync: null
                };
                callback(null);
            }

            var now = Date.now();
            var hour = 60 * 60 * 1000;
            syncStartDate = now;
            if(lastSyncInfo.locked && lastSyncInfo.syncStarted < (now - hour)) {
                console.log("aborting sync because something else is syncing");
                return callback(new Error("Sync Locked"));
            }
            callback(null);
        });
    });
}

function setSyncLock(callback) {
    lastSyncInfo.locked = true;
    lastSyncInfo.syncStarted = Date.now();
    db.ready(function(api) {
        api.salesforce.updateSyncInfo(lastSyncInfo, function(err, newInfo) {
            if(newInfo) {
                lastSyncInfo = newInfo;
            }
            callback(err || null);
        });
    });
}

function setSyncFinished(callback) {
    lastSyncInfo.locked = false;
    lastSyncInfo.lastSync = syncStartDate;
    db.ready(function(api) {
        api.salesforce.updateSyncInfo(lastSyncInfo, function(err, newInfo) {
            if(newInfo) {
                lastSyncInfo = newInfo;
            }
            callback(err || null);
        });
    });
}

/// get some info out of salesforce before we start
function getIndividualAccountId(callback) {
    var query = "SELECT Id FROM Account WHERE Name = 'Individual'";
    sf.query(query, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records && results.records.length > 0) {
            individualAccountId = results.records[0].Id;
            callback(null, individualAccountId);
        } else {
            callback(new Error("The 'Individual' Account does not exist"));
        }
    });
}

//// PROJECTS
// save loop - EEK!
function saveProject(project, delta, callback) {
    db.ready(function(api) {
        api.project.save(project, function(err) {
            if(err && err.message === 'conflict') {
                api.project.getById(project._id, function(err, proj) {
                    if(err) {
                        return callback(err);
                    }
                    mapper.applyDelta(proj, delta);
                    saveProject(proj, delta, callback);
                });
                return;
            }
            if(err) {
                return callback(err);
            }
            callback(null);
        });
    });
}

function syncProject(sfProject, callback) {
    db.ready(function(api) {
        api.project.getById(sfProject.Id, function(err, proj) {
            if(err && err.message !== 'missing') {
                return callback(err);
            }
            if(err && err.message === 'missing') {
                proj = api.project.blank();
            }
            var delta = mapper.projectDelta(sfProject, proj);
            mapper.applyDelta(proj, delta);
            saveProject(proj, delta, callback);
        });
    });
}

function syncProjects(callback) {
    var ldate = new Date(lastSyncInfo.lastSync);
    var projectQuery = queries.project + " WHERE LastModifiedDate > " + ldate.toISOString();
    sf.query(projectQuery, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records) {
            async.whilst(
                function () { return results.records.length !== 0; }, 
                function(cb) { syncProject(results.records.pop(), cb); }, 
                callback);
        } else {
            callback(null);
        }
    });
}

function syncFundingToProject(projectId, sfGrants, callback) {
    db.ready(function(api) {
        api.project.getById(projectId, function(err, proj) {
            if(err) {
                return callback(err);
            }
            var delta = mapper.fundingDelta(sfGrants, proj);
            mapper.applyDelta(proj, delta);
            saveProject(proj, delta, callback);
        });
    });
}

function syncFundingGrants(callback) {
    var ldate = new Date(lastSyncInfo.lastSync);
    var query = queries.designations + " WHERE LastModifiedDate > " + ldate.toISOString();
    sf.query(query, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records) {
            var groupByProject = _.groupBy(results.records, function(rec) {
                return rec.Changemaker_Project__r.Id;
            });
            var projectIds = Object.keys(groupByProject);
            async.whilst(
                function () { return projectIds.length !== 0; }, 
                function(cb) { 
                    var projectId = projectIds.pop();
                    syncFundingToProject(projectId, groupByProject[projectId], cb);
                }, 
                callback);
        } else {
            callback(null);
        }
    });
}

//// USERS
// save loop - EEK!
function saveUser(user, delta, callback) {
    db.ready(function(api) {
        api.user.save(user, function(err) {
            if(err && err.message === 'conflict') {
                api.user.get(user._id, function(err, usr) {
                    if(err) {
                        return callback(err);
                    }
                    mapper.applyDelta(usr, delta);
                    saveProject(usr, delta, callback);
                });
                return;
            }
            if(err) {
                return callback(err);
            }
            callback(null);
        });
    });
}

function syncSFUser(sfUser, callback) {
    db.ready(function(api) {
        var email = sfUser.Email;
        if(!email) {
            return callback(null);
        }
        api.user.getByEmail(email, function(err, user) {
            if(err && err.message !== 'missing') {
                return callback(err);
            }
            if(!user) {
                user = api.user.blank();
            }
            var delta = mapper.userDelta(sfUser, user);
            if(Object.keys(delta).length === 0) {
                return callback(null);
            }
            mapper.applyDelta(user, delta);
            saveUser(user, delta, callback);
        });
    });
}

function syncUsersFromSF(callback) {
    var ldate = new Date(lastSyncInfo.lastSync);
    // grab users from sales force and bring them down
    var userQuery = queries.people + " WHERE LastModifiedDate > " + ldate.toISOString();
    //console.log(userQuery);
    sf.query(userQuery, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        //console.log("results %j", results);
        if(results && results.records) {
            async.whilst(
                function () { return results.records.length !== 0; }, 
                function(cb) { syncSFUser(results.records.pop(), cb); }, 
                callback);
        } else {
            callback(null);
        }
    });
}

function upsertSFUser(user, callback) {
    var upsertParams = {};
    upsertParams.FirstName = user.first_name;
    upsertParams.LastName = user.last_name;
    upsertParams.Email = user.email;
    var method = "insert";
    if(user.salesforce_ref) {
        upsertParams.Id = user.salesforce_ref;
        method = "update";
    } else {
        upsertParams.AccountId = individualAccountId;
    }
    var newuser = nforce.createSObject('Contact', upsertParams);
    sf[method](newuser, oauth, function(err, resp) {
        if(err) {
            console.error(err);
            return callback(err);
        }
        if(method === 'insert') {
            user.salesforce_ref = resp.id;
            return saveUser(user, {salesforce_ref : resp.id}, callback);
        }
        callback(null, resp);
    });
}

function syncUsersFromCouch(callback) {
    var ldate = lastSyncInfo.lastSync;
    db.ready(function(api) {
        api.user.getByLastModified(ldate, function(err, users) {
            if(err) {
                return callback(err);
            }
            if(users.length === 0) {
                return callback(null);
            }
            // 3 cases here
            // 1. we have a salesforce_ref so we know who
            // 2. we have an email address, but we have to check if they are in SF
            // 3. they don't exist in SF
            // for now we're just going to merge all three and do an upsert
            async.whilst(
                function () { return users.length !== 0; }, 
                function(cb) { 
                    var kvp = users.pop(); 
                    upsertSFUser(kvp.value, cb); 
                }, 
                callback);
        });
    });
}

function syncUsers(callback) {
    async.series([syncUsersFromSF, syncUsersFromCouch], callback);
}

function startSync() {
    console.log("Started Sync");
    async.series([
        getLastSyncDate,
        setSyncLock,
        getIndividualAccountId,
        syncUsers,
        syncProjects,
        syncFundingGrants, 
        setSyncFinished], function(err, objs) {
            if(err) console.error(err);
            if(objs) console.log(JSON.stringify(objs));
            console.log("Ended Sync");
        });
}

login.connect(function(err, connectionDetails) {
    if(err) {throw err;}
    sf = connectionDetails.nforce;
    oauth = connectionDetails.oauth;
    startSync();
});
