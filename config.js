var url = require('url');

var publicPort = (process.env.PUBLIC_PORT || process.env.PORT || 8000);
var baseUri = (process.env.BASE_URI || 'http://localhost') +
            (publicPort === 80 ? '' : ':' + publicPort);

var redisUrl = process.env.REDISTOGO_URL || process.env.REDIS_URL;
redisUrl = redisUrl ? url.parse(redisUrl) : {};

module.exports = {
    // in `production` things will be less verbose
    environment: process.env.NODE_ENV || 'development',
    // HTTP port to listen on
    port: process.env.PORT || 8000,
    // no options mean connect to localhost
    // TODO: work out how we connect to the heroku redis provider
    redis: {
        host : redisUrl.host,
        port : redisUrl.port,
        pass : redisUrl.auth && redisUrl.auth.split(':')[1]
    },
    sessions: {
        //key: "spark.token",
        secret: process.env.SESSION_SECRET || 'purplemonkeydishwasherbatteryacid',
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 14 // 1000ms * 60s * 60m * 24h * 14d == 2 weeks
        }
    },
    couchdb: process.env.COUCHDB || process.env.CLOUDANT_URL || 'http://localhost:5984',
    facebook: {
        clientID: process.env.FB_APP_ID || console.log('facebook app id not set'),
        clientSecret: process.env.FB_APP_SECRET || console.log('facebook app secret not set'),
        callbackURL: baseUri + '/auth/facebook/callback'
    },
    salesforce: {
        clientId: process.env.SF_CLIENT_ID || '3MVG9Nvmjd9lcjRl1HX2Fn6HPz2dlpsVRIeQcsFj0qxdBUvYTin6620xf4dCsNdQVYF6SbJOcPqLdV5TycLYZ',
        clientSecret: process.env.SF_CLIENT_SECRET || '7909672720755772638',
        // not sur if this matters for server side things
        redirectUri: baseUri + '/oauth/_callback',
        environment: process.env.SF_ENV || 'sandbox',
        username: process.env.SF_USER_NAME || 'web@sparkinternational.org.webtestc',
        password: process.env.SF_PASSWORD || 'password666777',
        securityToken: process.env.SF_TOKEN || 'Be90LJgl0A4dXdwX9LvxQGGaQ'
    },
    braintree : {
        merchantID : process.env.BT_MERCHANT_ID || 'ykd6rtycbd6cmr6b',
        publicKey : process.env.BT_PUBLIC_KEY || 'sm7c77cnkwhj2vxf',
        privateKey : process.env.BT_PRIVATE_KEY || 'aa38e16f41693c304a5e6c12d3f340f0',
        clientSideEncryptionKey : process.env.BT_CLIENT_ENCRYPTION_KEY || 'MIIBCgKCAQEAzt0IxrM8Ox410Kt+LsolftlVDLnVC4CCcjIEuT0XwE87QGDwsnkMWamxeVxj7F1BBDTTSoQ/lvxdZ11UjMfWeI3Vxd7t8WbQCKY1IYyjCD8ks+Dw1cQW1jlXTQL9YTu9vNCq9KdPxQracLkr23kWVXzZdCd0fufSbmXYk8g3ZOmkttvc+IoMTZ3qVX3tGG+wzWPQMUcsleXlHU4mh8c1yUaCf1oihgGynUeXZrXgsFsvnlFOfTJ9xzZtIw8v0HQHiTyzSS7LbvI1HjIw6C3LYVho5UTccFyX4D/TIf71tpQaRU7g2eSLVNVJUHd+UCk+FaLjIB1UuQhRWX8meB4EnQIDAQAB'
    }
};

console.log('CouchDB host: ' + url.parse(module.exports.couchdb).host);
console.log('Redis host: ' + module.exports.redis.host);
