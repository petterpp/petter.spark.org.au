/*
 * GET home page.
 */
var projectRoutes = require("./project"),
    userRoutes =  require("./user"),
    authRoutes = require("./auth");

var db = require("../lib/db");

function renderIndexPage(req, res, next) {
    "use strict";
    var viewBag = {
        title: "foobar"
    };
    db.ready(function(api){
        api.project.categories(function(err, categories){
            if(err) { return next(err); }
            viewBag.categories = categories;
            res.render('index', viewBag);
        });
    });
}
module.exports.index = renderIndexPage;

function renderSparkInfoPage(req, res, next) {
    "use strict";
    var viewBag = {
        title: "Spark*"
    };
    res.render('spark', viewBag);
}

function renderSupporterInfoPage(req, res, next) {
    "use strict";
    res.send("/you");
}

function renderEndPovertyInfoPage(req, res, next) {
    "use strict";
    res.send("/end-extreme-poverty");
}

function notImplementedRoute(req, res, next) {
    "use strict";
    return next(new Error("This route is not implemented"));
}

function redirectToFilterPage(req, res, next) {
    "use strict";
    res.redirect("/change-maker");
}

function setupRoutes(app) {
    "use strict";
    app.get("/", renderIndexPage);
    app.get("/spark", renderSparkInfoPage);
    app.get("/you", notImplementedRoute, renderSupporterInfoPage);
    app.get("/end-extreme-poverty", notImplementedRoute, renderEndPovertyInfoPage);
    app.get("/change-maker", projectRoutes.list);
    app.get("/change-maker/country", redirectToFilterPage);
    app.get("/change-maker/country/:countrySlug", projectRoutes.listByCountry);
    app.get("/change-maker/category", redirectToFilterPage);
    app.get("/change-maker/category/:categorySlug", projectRoutes.listByCategory);
    app.get("/change-maker/country/:countrySlug/:categorySlug", projectRoutes.listByCountryAndCategory);
    app.get("/change-maker/:projectSlug", projectRoutes.getProject);
    app.get("/supporter/:userId", notImplementedRoute, userRoutes.userProfile);

    // authentication routes
    require("./auth").addAuthRoutes(app);
}
module.exports.setup = setupRoutes;
